<?php

namespace App\Http\Controllers;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    /*Method for list all categories
    *12-05-2023
    */

    public function categoryList()
    {
    	$categories = Category::get();
    	return view('admin/categories/list',['categories'=>$categories]);
    }
}
