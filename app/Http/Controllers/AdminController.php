<?php

namespace App\Http\Controllers;
use App\Http\MiddleWare;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Auth;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Redirect;

class AdminController extends Controller
{
	public function __construct() {
	   // $this->middleware("guest:admin");
	   //$this->middleware("auth:admin");
	}

    /*Method for admin login
    *13-05-2023
    */

    public function getLogin(){
		return view('admin/login');
	}

	/*Method for check admin login 
	*13-05-2023
	*/	
	public function postLogin(Request $request) {
	    //Credentials Array
	    
	    $credentials = ["email" => $request->email, "password" =>  bcrypt($request->password) ];
	    //Check For Valid Admin and Authenticate
		if (Auth::attempt($credentials)) {
			 return redirect(route("dashboard"));
		}
		else
		{
			return back()->with('Error', "Email or Password are Incorrects");
		}
	}


	/*Method for admin dashboard
	*13-05-2023
	*/	
	public function adminDashboard() {

		return view('admin/dashboard');

	}

}
