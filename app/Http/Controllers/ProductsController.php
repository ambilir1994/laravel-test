<?php

namespace App\Http\Controllers;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /*Method for list all products
    *12-05-2023
    */

    public function productList()
    {
    	$products = Product::get();
    	return view('admin/products/list',['products'=>$products,'alert'=>'']);
    }

    
    /*Method for add new product
    *12-05-2023
    */
    public function productAdd()
    {
    	$categories = Category::where('status',1)->get();

    	return view('admin/products/add',['categories'=>$categories]);
    }

    /*Method for store product
    *13-05-2023
    */
    public function productStore(Request $request)
    {
    	$product             = new Product;
    	$product->name       = $request->name;
    	$product->categories = json_encode($request->categories);
    	$product->price      = $request->price;
    	$product->quantity   = $request->quantity;
    	$product->status     = $request->status;

    	$path                = $request->file('image')->getRealPath();    
		$logo                = file_get_contents($path);
		$base64              = base64_encode($logo);
       
        $product->image      = 'image';
    	$product->save();

		return redirect(route('products'));
    }


     /*Method for edit product
    *13-05-2023
    */
    public function productEdit($id)
    {
    	$categories = Category::where('status',1)->get();

    	$product = Product::where('id',$id)->get();

    	return view('admin/products/edit',['product'=>$product,'categories'=>$categories]);
    }

    /*Method for delete product
    * 13-05-2023
    */
	public function productDelete($id){
		
		$product = Product::findOrFail($id);
		
		Product::destroy($id);

		return redirect(route('products'));
		
	}

}
