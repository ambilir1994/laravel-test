<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    use HasFactory;
    //Which fileds are going to filed on the login form 
    protected $fillable = [
         'email', 'password',
    ];
    //Hidden Fields (Password) for protecting your admin's data
    protected $hidden = [
        'password', 'remember_token',
    ];
    //Admin Model
    protected $table = "admin";

}
