<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::get('/','App\Http\Controllers\AdminController@getLogin')->name('logadmin');
Route::post('/','App\Http\Controllers\AdminController@postLogin');

Route::get('admin/dashboard','App\Http\Controllers\AdminController@adminDashboard')->name('dashboard');
Route::get('admin/categories','App\Http\Controllers\CategoriesController@categoryList')->name('categories');
Route::get('admin/products','App\Http\Controllers\ProductsController@productList')->name('products');
Route::get('admin/add-product','App\Http\Controllers\ProductsController@productAdd')->name('addProduct');
Route::post('admin/add-product','App\Http\Controllers\ProductsController@productStore')->name('storeProduct');
Route::get('admin/edit-product/{id}','App\Http\Controllers\ProductsController@productEdit')->name('editProduct');
Route::get('admin/product/{id}/delete','App\Http\Controllers\ProductsController@productDelete')->name('deleteProduct');
