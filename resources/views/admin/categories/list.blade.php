 
@extends('admin/layout/default')
@section('title', 'Dash board')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Product Categories</h1>
    
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
       
        <div class="card-body">
           
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Name</th>                           
                            <th>Status</th>
                        </tr>
                    </thead>
                    
                        
                    <tbody>
                        @if($categories)
                            @foreach($categories as $cat)
                                <tr>
                                    <td>{{ $cat->name }}</td>
                                   
                                    <td>@if($cat->status == 1) Active @else Inactive @endif</td>
                                    
                                </tr>                           
                            @endforeach
                        @else

                            <tr>No Results Found</tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->
@endsection