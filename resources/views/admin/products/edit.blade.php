@extends('admin/layout/default')
@section('title','Add New Product')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Products</h1>
    
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
       
        <div class="card-header py-3">
            <a href="{{ route('addProduct') }}"><button class="btn btn-success">Edit Product</button></a>
        </div>
        <div class="card-body">
            <form method="post" action="{{ route('storeProduct') }}" enctype="multipart/form-data">
			    @csrf
			    
                <div class="form-group">
                    <label>Name</label> 
                    <input type="text" placeholder="Enter Product " class="form-control"  name="name" value="{{ $product[0]->name }}" required>
                </div>

                <div class="form-group">
                    <label>Image</label>
                    <div class="col-lg-8">
                        <img src=""  class="thumbnail" value="">
                       
                        <input type="file" name="image" class="banner_image" accept="image/*"/>                                     
                    </div>
                </div>

                <div class="form-group">
                    <label>Catgeories</label> 
                    <select  multiple class="chosen-select form-control" required="" name="categories[]">
                        <option>--Please Select--</option>
                        @foreach($categories as $cat)
                            <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                        @endforeach
                    </select>
                    
                </div>
                <div class="form-group">
                    <label>Price</label> 
                    <input type="number" min="1" step="any"  placeholder="Price" class="form-control" name="price" value="{{ $product[0]->price }}" required>
                </div>
                
                <div class="form-group">
                    <label>Quantity</label> <br/>
                    <input type="number" placeholder="Quantity" class="form-control"  name="quantity" value="{{ $product[0]->quantity }}" required  min="1">
                </div>

                <div class="form-group">
                    <label>Status</label> <br/>
                    <select class="form-control" name="status">
                        <option value="draft" @if($product[0]->status == 'draft') Selected @endif>Draft</option>
                        <option value="published" @if($product[0]->status == 'published') Selected @endif>Published</option>
                    </select>
                </div>

                
                <div class="col-lg-12">
                    <div class="hr-line-dashed"></div>
                    <button class="btn btn-sm btn-primary m-t-n-xs" type="submit"><strong>Submit</strong></button>
                </div>
                <div class="clearfix"></div>
            </form>                 
        </div>
    </div>
</div>
<script type="text/javascript">
$(function() {
    $(".chosen-select").chosen();
});
</script>
@endsection
