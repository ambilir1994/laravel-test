 
@extends('admin/layout/default')
@section('title', 'Dash board')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Products</h1>
    
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
       
        <div class="card-header py-3">
            <a href="{{ route('addProduct') }}"><button class="btn btn-success">Add New</button></a>
        </div>
        <div class="card-body">
           
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Image</th>
                            <th>price</th>
                            <th>Quantity</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    
                        
                    <tbody>
                        @if($products)
                            @foreach($products as $product)
                                <tr>
                                    <td>{{ $product->name }}</td>
                                    <td><img src=""></td>
                                    <td>{{ $product->price}}</td>
                                    <td>{{ $product->quantity }}</td>
                                    <td>{{ $product->status }}</td>
                                    <td>                                    
                                        <a href="{{ URL::to('admin/edit-product/'.$product->id) }}"><i class="fa fa-edit "></i></a>&emsp;
                                        <a href="{{URL::to('/admin/product/' . $product->id . '/delete')}}" onclick="event.stopPropagation();return confirm('Do you really want to delete this category?');" <i class="fa fa-trash "></i></a>
                                    </td>
                                </tr>                           
                            @endforeach
                        @else

                            <tr>No Results Found</tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->
@endsection